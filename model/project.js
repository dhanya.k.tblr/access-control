const mongoose=require("mongoose")
const Schema=mongoose.Schema
const projects=new Schema ({
    name:{type:String,required:true},
    user_id:{type: mongoose.Schema.Types.ObjectId, ref: 'users'}
})
module.exports=mongoose.model("projects",projects)