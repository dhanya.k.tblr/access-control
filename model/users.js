const mongoose = require("mongoose")
const Schema = mongoose.Schema
let users = new Schema({
    name: { type: String, required: true },
    salt:{type:String,required:true},
    password: { type: String, required: true },
    role: { type: mongoose.Schema.Types.ObjectId, ref: 'Role' }
})
module.exports = mongoose.model("users", users)