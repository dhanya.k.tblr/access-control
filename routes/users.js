const express=require("express")
const {authorize}=require('../helpers/authorize')
const {create,signin,getUsers} =require("../controller/userscon")
const {verifyToken}=require('../helpers/middlewares')
const router=express.Router()
router.post("/create",create)
router.post("/signin",signin)
router.get("/getusers",verifyToken,getUsers)
module.exports=router
