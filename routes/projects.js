const express=require("express")
const {verifyToken,canAccessProject}=require('../helpers/middlewares')
const {create,getProjects} =require("../controller/projectcon")
const router=express.Router()
router.post("/create",create)
router.get("/getprojects",verifyToken,canAccessProject,getProjects)

module.exports=router
