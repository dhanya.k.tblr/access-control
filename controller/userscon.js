const userModel = require("../model/users")
const Role = require("../model/roles.js")
const bcrypt=require("bcrypt")
const encryptPassword = require('../helpers/passwordEncryption');
const jwt = require('jsonwebtoken');
module.exports = {
    //create user
    create: async (req, res, next) => {
        try {
            let fields = ["name", "role"]
            fields.map((item) => {
                if (!req.body[`${item}`])
                    throw { message: `${item}"is required"`, code: 400 }
            })
            let dbuser = await userModel.findOne({ 'name': req.body.name })
            if (dbuser) return res.send({ message: "User exists" });
            else {
                const rslt = await encryptPassword(req.body.password);
                let data = new userModel(req.body)
                data.salt = rslt.salt
                data.password = rslt.hash
                data.save((err, users) => {
                    if (!err) {
                        let userdetails = {
                            message: "data inserted",
                            users
                        }
                        return res.send(userdetails);
                    }
                    else {
                        let userdetails = { message: 'Can not add user', code: 400 }
                        return res.send(userdetails);
                    }
                });
            }
        } catch (e) {
            res.status(e.code).json({ message: e.message });
        }
    },
    //log in
    signin: async (req, res) => {
        try {
            let fields = ["name", "password"]
            fields.map((item) => {
                if (!req.body[`${item}`])
                    throw { message: `${item}"is required"`, code: 400 }
            })
            let dbuser = await userModel.findOne({ 'name': req.body.name }).populate("role")
            if (dbuser) {
                const hashedpwd = await bcrypt.hash(req.body.password, dbuser.salt);
                if (hashedpwd == dbuser.password) {
                    let result = {
                        id: dbuser.id,
                        name: dbuser.name,
                        role_name:dbuser.role.role_name
                    }
                    let token = jwt.sign(result, "secretkey", { expiresIn: 86400 });
                    let userdetails = {
                        message: "true",
                        id: dbuser.id,
                        name: dbuser.name,
                        token: token,
                    }
                    res.status(200).json(userdetails);
                }
                else{
                    res.status(400).json({message:"Authentication false"});
                }
            }
            else{
                res.status(400).json({message:"User not exists" });
            }

        }
        catch (e) {
            res.status(e.code).json({ message: e.message });
        }

    },
    //get users
    getUsers: async (req, res, next) => {
        try {
            let condition = {}
            let attribute = ['_id','name']
            if (req.query.id)
                condition = { _id: req.query.id }
               let result = await userModel.find(
                condition
            ).select(attribute)
                .populate({ path: 'role', Model: Role })
                .exec()
            return res.send(result)
        } catch (e) {
            res.status(e.code).json({ message: e.message });
        }
    }


}