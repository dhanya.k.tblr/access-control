const projectModel = require("../model/project")
module.exports = {
    create: async (req, res, next) => {
        try {
            let fields = ["name", "user_id"]
            fields.map((item) => {
                if (!req.body[`${item}`])
                    throw { message: `${item}"is required"`, code: 400 }
            })
            let dbuser = await projectModel.findOne({ 'name': req.body.name })
            if (dbuser) return res.send({ message: "Project exists" });
            else {
                let data = new projectModel(req.body)
                data.save((err, projects) => {
                    if (!err) {
                        let projectdetails = {
                            message: "data inserted",
                            projects
                        }
                        return res.send(projectdetails);
                    }
                    else {
                        let projectdetails = { message: 'Can not add project', code: 400 }
                        return res.send(projectdetails);

                    }
                });
            }
        }
        catch (e) {
            res.status(e.code).json({ message: e.message });
        }
    },
    //get projects
    getProjects: (req, res, next) => {
        try {
            let condition = {};
            let pro = {}
            let attribute = ['-password', '-salt']
            if (req.isAdmin) {
                if (req.query.id && req.isAdmin) {
                    projectModel.find({ _id: req.query.id },
                        function (err, projects) {
                         pro = projects
                        })
                        if (pro)
                        condition = { _id: req.query.id }
                        if(Object.keys(pro).length==0) return res.send({ message: "No project found" })
                }
                else condition = {}
            }
            else {
                if (req.query.id && (req.isAdmin == false)) {
                    projectModel.find({ $and: [{ _id: req.query.id }, { user_id: req.user.id }] }
                        , function (err, projects) {
                            pro = projects
                        })
                    if (pro)
                        condition = { _id: req.query.id }
                        
                    if(Object.keys(pro).length==0) return res.send({ message: "No project found" })
                }
                else condition = { user_id: req.user.id }
            }
            projectModel.find(condition).populate('user_id', attribute)
                .exec(function (err, projects) {
                    return res.send(projects)
                })
        } catch (e) {
            res.status(e.code).json({ message: e.message });
        }
    }
}
