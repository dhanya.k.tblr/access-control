const jwt = require("jsonwebtoken");

module.exports = {
    verifyToken: (req, res, next) => {
        const authHeader = req.headers.authorization;
        if (authHeader == undefined) {
            res.status(401).send({ error: "No token provided" });
        }
        let token = authHeader
        jwt.verify(token, "secretkey", function (err, decoded) {
            if (err) {
                res.status(401).send({ error: "Authentication failed" });
            }
            else {
                req.user = decoded;
                }
        }); next();
    },
    canAccessProject:async (req, res, next) => {
           if(req.user.role_name=="admin")
           req.isAdmin=true;
           else  req.isAdmin=false;
           next()
        }
       
    


}
