var bcrypt = require('bcrypt');

const encryptPassword = async password => {
    let salt= await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, salt);
    let rslt = {salt : salt, hash : hash}
    return rslt
}
module.exports=encryptPassword;