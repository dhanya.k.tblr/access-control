module.exports = {
    authorize: (role) => {
        return async (req, res, next) => {
            console.log(req.user, "............")
            if (role.includes(req.user.role_name)) {
                next();
            }
            else {
                res.status(403).json({ error: "Permission Denied" });
            }
        }
    }
}