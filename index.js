const express=require("express")
const app=express()
const http=require("http").Server(app)
const PORT=process.env.PORT || 3000
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


require('./config/database')
const userRouter=require("./routes/users")
const projectRouter=require("./routes/projects")

app.use("/users",userRouter )
app.use("/projects",projectRouter)

http.listen(PORT, ()=>{
    console.log(`App listening at ${PORT}`)
})